# Script Name: tea_time_lm.R
# Purpose: Linear Regression Tutorial 
# Authors: Amit Kumar Verma

# install a collection of datasets
if (!require("datasets")) install.packages("datasets");
library(datasets)

# View the "mtcars" data frame
View(mtcars)

# Expose the columns of "mtcars" data set. Without this
# line of code one would have to access columns by
# mtcars$<name> where <name> is the column name. For
# instance: mtcars$mpg
attach(mtcars)

###################################
## Standard Deviation            ##
###################################

# mean : constant-only (zero-variable) regression model
# value around which the mean squared deviation of the data is minimized

range(wt)
sd(wt) #  measure of dispersion of the data from the mean

###################################
## Normal Distribution           ##
###################################

plot(seq(-3.2,3.2,length=50),dnorm(seq(-3,3,length=50),0,1),type="l",xlab="",ylab="",ylim=c(0,0.5))
segments(x0 = c(-3,3),y0 = c(-1,-1),x1 = c(-3,3),y1=c(1,1))
text(x=0,y=0.45,labels = expression("99.7% of the data within 3" ~ sigma))
arrows(x0=c(-2,2),y0=c(0.45,0.45),x1=c(-3,3),y1=c(0.45,0.45))
segments(x0 = c(-2,2),y0 = c(-1,-1),x1 = c(-2,2),y1=c(0.4,0.4))
text(x=0,y=0.3,labels = expression("95% of the data within 2" ~ sigma))
arrows(x0=c(-1.5,1.5),y0=c(0.3,0.3),x1=c(-2,2),y1=c(0.3,0.3))
segments(x0 = c(-1,1),y0 = c(-1,-1),x1 = c(-1,1),y1=c(0.25,0.25))
text(x=0,y=0.15,labels = expression("68% of the data within 1" * sigma),cex=0.9)

###################################
## Simple Linear Regression      ##
###################################

# Plot weight vs. mpg
plot(wt, mpg)

# Create a linear model
model = lm(mpg ~ wt)

#################################
## mpg is y, wt is x           ##
## mpg = a + b*wt + e          ##
## e is error term             ##  
#################################

# Summary of the Regression
summary(model)

# Estimate represents value of the coefficients (e.g. a and b)
coefficients(model)

# std. Error defines the confidence level
confint(model, level=0.95)

# Add a straight line to the plot
abline(model)

##################################
## Actual vs Predicted Values   ##
##################################

# Base Plot with defined axes limits
plot(wt, mpg, pch=20, col="red", xlab="Weight", ylab="Miles per gallon", ylim=c(8,34)) 
# Linear Regression Model
par(new=TRUE)
abline(model)
# Predicted values from the model
par(new=TRUE)
plot(wt, fitted(model), pch=16, col="green", xlab="", ylab="", xaxt="n", yaxt="n", ylim=c(8,34))
# Segments represent the residuals 
par(new=TRUE)
segments(x0 = wt, y0 = mpg, x1 = wt, y1 = fitted(model), col = "red")

######################################
## residual(i) = Actual - Predicted ##
## i represents i th element        ##
## RSS - Residual Sum of Squares    ## 
######################################

plot(model)


# Summary of the Regression
summary(model)

# Standard error: the standard deviation of the noise in Y

##################################
##  Assessing the Accuracy      ##
##################################

plot(wt, mpg) # Doesn't look linear!

# Error term is a catch-all for what we miss with this simple model
# There may be other variables that cause variation in Y
# Typical assumption is that the error term is independent of X

# Summary of the Regression
summary(model)

# p-value determines the Null Hypothesis
# RSE: estimate of the standard deviation of e, absolute measure the lack of fit
# or average amount that the response will deviate from the true regression line
# R-square: proportion of variance explained [(TSS-RSS)/TSS]

###################################
## Multiple Linear Regression    ##
###################################

# Create a multiple variable linear model
model2 = lm(mpg ~ wt + hp) # [mpg = a + b*wt + c*hp + e ]

# Summary of the Regression
summary(model2)

summary(model)

# F-stat: Is There a Relationship Between the Response and Predictors?
# when there is no relationship, one would expect a value close to 1
# When n is large, just a little larger than 1 might provide evidence against Null Hypothesis
# In contrast, a larger F-statistic is needed to reject H0 if n is small

# Adjusted R-square: pays a price for the inclusion of unnecessary variables in the model


#######################################################
## Diagnostic Plots: For identifying non-linearity   ##
#######################################################

# residuals are normally distributed
# variance doesn’t change as a function of the dependent variable

plot(model)

# Plot1: whether there is any systematic variation of the residuals with the fitted values
# Plot2: whether residuals are normally distributed
# Plot3: if the standard deviation among the residuals appears to be about constant
# Plot4: if there were any overly influential points

# Plotting all plots simultaneously 

par(mfrow = c(2,2))
plot(model)

########################
## Comparing Models   ##
########################

AIC(model, model2) # Akaike's Information Criterion
BIC(model, model2) # Bayesian Information Criterion

#########################
## Variable Selection  ##
#########################

# Let's look at the variables

library(psych)
pairs.panels(mtcars, ellipses = FALSE) 


attach(mtcars)

###################################

par(mfrow = c(1,1))
# Plot weight vs. mpg
plot(wt, mpg)

# Create a Polynomial Model model
model.poly = lm(mpg ~ poly(wt, 2))
# Summary of the Regression
summary(model.poly)
summary(model)

# Estimate represents value of the coefficients (e.g. a and b)
coefficients(model)


plot(wt, mpg, pch=20, col="red", xlab="Weight", ylab="Miles per gallon", ylim=c(8,34)) 
# Linear Regression Model
par(new=TRUE)

# Predicted values from the model
par(new=TRUE)
plot(wt, fitted(model.poly), pch=16, col="green", xlab="", ylab="", xaxt="n", yaxt="n", ylim=c(8,34))
# Segments represent the residuals 
par(new=TRUE)
segments(x0 = wt, y0 = mpg, x1 = wt, y1 = fitted(model.poly), col = "red")


## Multiple Variable Model


model.mult <- lm(mpg ~ wt + disp + drat)
summary(model.mult) 

# Lets plot this, versus the first model
par(mfrow = c(1,2)) # Make room for two plots


# Not really a good thing to do, since we have more dimensions than weight

plot(wt, mpg, pch=20, col="red", xlab="Weight", ylab="Miles per gallon", ylim=c(8,34)) 
# Predicted values from the model
par(new=TRUE)
plot(wt, fitted(model.mult), pch=16, col="green", xlab="", ylab="", xaxt="n", yaxt="n", ylim=c(8,34))
# Segments represent the residuals 
par(new=TRUE)
segments(x0 = wt, y0 = mpg, x1 = wt, y1 = fitted(model.mult), col = "red")



# Plotting the previous model
plot(wt, mpg, pch=20, col="red", xlab="Weight", ylab="Miles per gallon", ylim=c(8,34)) 
# Linear Regression Model
par(new=TRUE)
abline(model)
# Predicted values from the model
par(new=TRUE)
plot(wt, fitted(model), pch=16, col="green", xlab="", ylab="", xaxt="n", yaxt="n", ylim=c(8,34))
# Segments represent the residuals 
par(new=TRUE)
segments(x0 = wt, y0 = mpg, x1 = wt, y1 = fitted(model), col = "red")




# Interaction Between 
par(mfrow = c(1,1))

# Model All Terms
model.interaction = lm(mpg ~ . ,data=mtcars)
summary(model.interaction)

# Except hp
model.interaction = lm(mpg ~ . - hp ,data=mtcars)
summary(model.interaction)

# Each term multipled by another
model.interaction = lm(mpg ~ .*. ,data=mtcars)
summary(model.interaction)

# All interaction terms minus each term indiciually
model.interaction = lm(mpg ~ .*. - .,data=mtcars)
summary(model.interaction)

# Just interaction of a couple terms
model.interaction = lm(mpg ~ I(wt*hp),data=mtcars)
summary(model.interaction)


# Stepwise Regression - Automating Optimal Fitting

library(MASS)
fit <- lm(mpg ~ 1)
step <- stepAIC(fit, direction = "both", scope = ~ cyl + disp + hp + drat 
                + wt + qsec + vs + am + gear + carb) 
step$anova # display results


model3 <- lm(mpg ~ wt + cyl + hp)
summary(model3) 

model3 <- lm(mpg ~ wt + cyl) # Removed hp
summary(model3)


### Make some variable changes

fit <- lm(mpg ~ 1)
step <- stepAIC(fit, direction = "both", scope = ~ log(cyl) + log(disp) + hp + drat 
                + wt + qsec + vs + am + gear + carb) 
step$anova # display results


model3 <- lm(mpg ~ wt + cyl + hp)
summary(model3) 

model3 <- lm(mpg ~ wt + cyl) # Removed hp
summary(model3)

plot(step)



#### Backwards Feature Selection
install.packages("caret")
library(caret)

# Big difference here is bootstrap sampling

lmProfile <- rfe(mtcars[,2:6], mtcars$mpg, sizes = c(1:4),
                 rfeControl = rfeControl(functions = lmFuncs, number = 200))

lmProfile
summary(lmProfile)
plot(lmProfile, type = "l")
lmProfile$fit

# More variables
lmProfile <- rfe(cbind(mtcars[,2:7], mtcars[,9:11]), mtcars$mpg, sizes = c(1:4),
                 rfeControl = rfeControl(functions = lmFuncs, number = 200))

lmProfile
summary(lmProfile)
plot(lmProfile, type = "l")











# Full in Depth Example from the ?rfe help file

data(BloodBrain)

x <- scale(bbbDescr[,-nearZeroVar(bbbDescr)])
x <- x[, -findCorrelation(cor(x), .8)]
x <- as.data.frame(x)

set.seed(1)
lmProfile <- rfe(x, logBBB,
                 sizes = c(2:25, 30, 35, 40, 45, 50, 55, 60, 65),
                 rfeControl = rfeControl(functions = lmFuncs, 
                                         number = 200))
set.seed(1)
lmProfile2 <- rfe(x, logBBB,
                  sizes = c(2:25, 30, 35, 40, 45, 50, 55, 60, 65),
                  rfeControl = rfeControl(functions = lmFuncs, 
                                          rerank = TRUE, 
                                          number = 200))

xyplot(lmProfile$results$RMSE + lmProfile2$results$RMSE  ~ 
         lmProfile$results$Variables, 
       type = c("g", "p", "l"), 
       auto.key = TRUE)

rfProfile <- rfe(x, logBBB,
                 sizes = c(2, 5, 10, 20),
                 rfeControl = rfeControl(functions = rfFuncs))

bagProfile <- rfe(x, logBBB,
                  sizes = c(2, 5, 10, 20),
                  rfeControl = rfeControl(functions = treebagFuncs))

set.seed(1)
svmProfile <- rfe(x, logBBB,
                  sizes = c(2, 5, 10, 20),
                  rfeControl = rfeControl(functions = caretFuncs, 
                                          number = 200),
                  ## pass options to train()
                  method = "svmRadial")


