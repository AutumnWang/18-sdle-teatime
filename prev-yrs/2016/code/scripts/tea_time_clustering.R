# Script Name: tea_time_clustering.R
# Purpose: clustering analysis Tutorial 
# Authors: Mohammad Akram Hossain
# License: Creative Commons Attribution-ShareAlike 4.0 International License.
##########

# R code goes below
## load the data set
df = iris
species = df$Species ## seperate species column
df = df[,-5] ## remove species column
summary(species) 


## k-means clustering ##
## k-means clustering aims to partition n observations into k clusters
##in which each observation belongs to the cluster 
##with the nearest mean, serving as a prototype of the cluster.  

### determine how many cluster do we need ###

## function for calculation sum of within groups ##
wssplot <- function(data, nc=15, seed=1234){
  wss <- (nrow(data)-1)*sum(apply(data,2,var)) ## sum of squares
  for (i in 2:nc){
    set.seed(seed)
    wss[i] <- sum(kmeans(data, centers=i)$withinss)} ## within group sum of squares
  plot(1:nc, wss, type="b", xlab="Number of Clusters",
       ylab="Within groups sum of squares")} #plot the results

wssplot(df, nc=6) ##call function and plot, find elbow points


## kmeans cluster ###
## kmeans(dataframe, number_of_cluster,iter.max)
set.seed(1234)
k.means.fit <- kmeans(df[,3:4], 3,iter.max=100) 
attributes(k.means.fit)
k.means.fit$cluster
k.means.fit$size
plot(iris[,1], iris[,2], col=k.means.fit$cluster)

points(k.means.fit$centers[,c(1,2)], col=1:3, pch=8, cex=2)
table(k.means.fit$cluster, iris$Species)



### plotting k-means cluster with pca ###
library(cluster)
par(mfrow=c(1,1))
clusplot(df, k.means.fit$cluster, main='2D representation of the Cluster solution',
         color=TRUE, shade=TRUE,
         labels=2, lines=0)

### hierarchial clustering ###
d <- dist(df, method = "euclidean") # Euclidean distance matrix.
H.fit <- hclust(d, method="ward")
plot(H.fit) # display clustering dendogram

?hclust

groups <- cutree(dend, k=3) # cut tree into 3 clusters
# draw dendogram with red borders around the 3 clusters
rect.hclust(dend, k=3, border="red") 

library(colorspace)

install.packages('dendextend')
library(dendextend)
d <- dist(df,method="manhattan") # method= manhattan 
hc_iris <- hclust(d, method = "average")
dend <- as.dendrogram(hc_iris)
plot(dend)

# Color the branches based on the clusters:
dend <- color_branches(dend, k=3)

iris_species <- rev(levels(species))

dend <- rotate(dend, 1:150)


dend <- color_branches(dend, k=3) #

# Manually match the labels, as much as possible, to the real classification of the flowers:
labels_colors(dend) <-
  rainbow_hcl(3)[sort_levels_values(
    as.numeric(iris[,5])[order.dendrogram(dend)]
  )]

# We shall add the flower type to the labels:
labels(dend) <- paste(as.character(iris[,5])[order.dendrogram(dend)],
                      "(",labels(dend),")", 
                      sep = "")
# We hang the dendrogram a bit:
dend <- hang.dendrogram(dend,hang_height=0.1)
# reduce the size of the labels:
dend <- set(dend, "labels_cex", 0.6)
# And plot:
par(mar = c(3,3,3,7))
plot(dend, 
     main = "Clustered Iris data set
     (the labels give the true flower species)", 
     horiz =  TRUE,  nodePar = list(cex = .007))
legend("topleft", legend = iris_species, fill = rainbow_hcl(3))


par(mfrow=c(3,1))
mm = c ('euclidean','manhattan','maximum')
for (i in 1:3){
  x =  mm[i]
  d <- dist(df,method=x) # method= manhattan 
  hc_iris <- hclust(d, method = "average")
  #dend <- as.dendrogram(hc_iris)
  cat('plotting dendrogram')
  plot(hc_iris)
  groups <- cutree(hc_iris, k=3) # cut tree into 3 clusters
  # draw dendogram with red borders around the 3 clusters
  rect.hclust(hc_iris, k=3, border="red") 
  
  
}

### heatmap clustering ##
library(gplots)
  
heatmap.2(as.matrix(df), 
                  main = "Heatmap for the Iris data set",
                  srtCol = 20,
                  dendrogram = "row",
                  Rowv = dend,
                  Colv = "NA", # this to make sure the columns are not ordered
                  trace="none",          
                  margins =c(5,0.1),      
                  key.xlab = "Cm",
                  denscol = "grey",
                  density.info = "density",
                  RowSideColors = rev(labels_colors(dend)), # to add nice colored strips        
                  )
