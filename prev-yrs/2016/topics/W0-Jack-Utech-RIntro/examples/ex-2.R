z <- c(10, -3, 8, -7)

z * z > 8

z[z * z > 8] 

s <- seq(1, 8, by = 0.25)

subset(s, s > 3)

which(s > 3)

which(!(s > 3))

first_ten <- seq(1:10) 

first_ten[first_ten %% 2 == 0]

###############################################################################
###############################################################################
###############################################################################

class(1)

class("string")

class(TRUE)

class(NA)

class(NULL)

is.null("string")

is.character("string")

is.numeric(2)

class(as.numeric("12345"))

class(as.numeric("abcde"))

