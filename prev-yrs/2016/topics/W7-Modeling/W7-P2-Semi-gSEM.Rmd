---
title: "sunfarm"
author: "Yu Wang"
date: "July 26, 2016"
output: html_document
---

```{r}
library(sgSEM)
#readin the data
rdata = read.csv("V:/vuv-data/proj/Abdul-BS/DataFrame/DataFrame-v03.csv")
ndata = rdata[rowSums(is.na(rdata)) == 0,]
panel.cor <- function(x, y, digits = 2, cex.cor, ...)
{
  usr <- par("usr"); on.exit(par(usr))
  par(usr = c(0, 1, 0, 1))
  # correlation coefficient
  r <- cor(x, y)
  txt <- format(c(r, 0.123456789), digits = digits)[1]
  txt <- paste("r= ", txt, sep = "")
  text(0.5, 0.6, txt)
}

pairs(ndata[,3:20],lower.panel = panel.smooth, upper.panel = panel.cor, main = "Pairs Plot of Backsheet Data")

library(psych)
pairs.panels(ndata,ellipses = FALSE)
data1 = ndata[1:18,]
data2 = subset.data.frame(ndata, Material == "TPT" & Exposure == "CyclicQUV")

pairs(data1[,5:27],lower.panel = panel.smooth, upper.panel = panel.cor, main = "Pairs Plot of Backsheet Data: Material: TPT, Exposure: HotQUV")
#build model for YI, TPT, HOTQUV
lm1.1 = lm(YI~Dose + abs337 + abs415 +ftir1025 +ftir1088 + ftir1141 + ftir11163 + ftir1230 + ftir1364 +ftir1409 +ftir1424 +ftir1737,data = data1)
summary(lm1.1)
library(MASS)
step = stepAIC(lm1.1,direction = "backward")
step$anova
lm1.1 =lm(YI ~ Dose + abs337 + abs415 + ftir1088 + ftir11163 + ftir1364 + 
    ftir1737, data = data1)
summary(lm1.1)
#build model for Gloss60, TPT, HOTQUV
lm1.2 = lm(gloss60~Dose + abs337 + abs415 +ftir1025 +ftir1088 + ftir1141 + ftir11163 + ftir1230 + ftir1364 +ftir1409 +ftir1424 +ftir1737,data = data1)
summary(lm1.2)

step = stepAIC(lm1.2,direction = "backward")
step$anova
lm1.2 =lm( gloss60~ abs337 + abs415 + ftir1025 + ftir1088 + ftir1141 + 
    ftir1230 + ftir1364 + ftir1409 + ftir1737, data = data1)
summary(lm1.2)

pairs(data2[,5:27],lower.panel = panel.smooth, upper.panel = panel.cor, main = "Pairs Plot of Backsheet Data: Material: TPT, Exposure: CyclicQUV")
lm2.1 = lm(YI~Dose + abs337 + abs415 +ftir1025 +ftir1088 + ftir1141 + ftir11163 + ftir1230 + ftir1364 +ftir1409 +ftir1424 +ftir1737,data = data2)
summary(lm2.1)

step = stepAIC(lm2.1,direction = "backward")
step$anova
lm2.1 =lm(YI ~ Dose + abs415 + ftir1141 + ftir11163 + ftir1230, data = data1)
summary(lm2.1)
```
Although the adjusted R square is high and near 1, the P value of most variables are so large that cannot being receivable.

Therefore, using semi-gSEM to buile the model:

*Stress: Time

*Mechanism: Absortion at 337nm --- absorption due to degradation byproduct
            FTIR at 1025 cm^(-1) --- Crytallization
            FTIR at 1163 cm^(-1) --- Photo-oxidation
            FTIR at 1737 cm^(-1) --- Chain Scission

*Response: YI/Gloss


```{r}

ans1.1 = data1[,c(6,11,15,17,25)]
colnames(ans1.1) = c("Time","YI","Absortion","Crystallization","PhotoOxidation","ChainScission")
sem1.1 = sgSEM(ans1.1)
plot(sem1.1,main = "semi-gSEM diagram for TPT backsheet under HotQUV Exposure:YI as the main response")

ans1.2 = data1[,c(6,13,15,22,25)]
colnames(ans1.2) = c("Time","Gloss60","Absortion","Oxidation","OxidationProduct")
sem1.2 = sgSEM(ans1.2)
plot(sem1.2, main = "semi-gSEM diagram for TPT backsheet under HotQUV Exposure:Gloss as the main response")

mod1 = lm(Gloss60~I(Time^2)+Time, data = ans1.2)
summary(mod1)
mod1.1 = lm(Gloss60~I(Absortion^2)+Absortion, data = ans1.2)
summary(mod1.1)
mod1.2 = lm(Absortion~I(Time^2)+Time, data = ans1.2)
summary(mod1.2)
```
The chain scission and photo oxidation are found to be related to YI formation, but both with a relatively small fitting power. And on contrary to what is expected, there is no correlation was found bwteen YI and time, abosoption and the crystallization the evidence of degradation.
The value of R^2 of crystallization and photo oxidation between time is bigger than that of chain scission and absoption, and the value of R^2 of absorption and chain scission between gloss is bigger.

Gloss vs.Time: Gloss = -3.489e-07T^2 + 1.271e-03T + 6.403e+00
Gloss vs. Absorptio: Gloss = -2184.68(A^2) + 4612.4A - 2427.2
Absorption vs. Time: Absorption = 9.689e-10T^2 - 1.334e-05T + 1.074e+00 
Gloss ~ Absorption ~ Time: Gloss = -2184.68(9.689e-10T^2 - 1.334e-05T + 1.074e+00) + 4612.4(9.689e-10T^2 - 1.334e-05T + 1.074e+00) - 2427.2
```{r}
ans2.1 = data2[,c(6,11,15,17,20,25)]
colnames(ans2.1) = c("Time","YI","Absortion","Crystallization","PhotoOxidation","ChainScission")
sem2.1 = sgSEM(ans2.1)
plot(sem2.1,main = "semi-gSEM diagram for TPT backsheet under CyclicQUV Exposure:YI as the main response")

ans2.2 = data2[,c(6,13,15,24,25)]
colnames(ans2.2) = c("Time","Gloss60","Absortion","Oxidation","OxidationProduct")
sem2.2 = sgSEM(ans2.2)
plot(sem2.2, main = "semi-gSEM diagram for TPT backsheet under CyclicQUV Exposure:Gloss as the main response")

mod2 = lm(Gloss60~I(Time^2)+Time, data = ans2.2)
summary(mod2)
mod2.1 = lm(Gloss60~I(ChainScission^2)+ChainScission, data = ans2.2)
summary(mod2.1)
mod2.2 = lm(ChainScission~I(Time^2)+Time, data = ans2.2)
summary(mod2.2)

```

