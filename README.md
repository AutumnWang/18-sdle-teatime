# 18-sdle-teatime

This repository contains all the talks and documents for summer 2018 tea-time sessions.

## SDLE Tea-times: 2:30 to 3:15, on Tuesdays, Wednesdays and Thursdays in White 411.
## Starting Tuesday June 12th, through Thursday July 19th, 2017
## A set of teachings of Data Science topics such as Data Science Infrastructure, Git, Linux, R, Python, HPC.
### Any interested people are invited. 
### These are intended to be simple startups in these topics.
### These are relatively informal, but structured. 

## Here we will share datasets, R codes and markdown reports. 
### SDLE Tea-time Videos are on [Periscope (Live)] [9] and [YouTube] [10]
## This is the third year of Tea-time, for files from previous years, look in the subfolder 'prev-yrs'.

## Project Title: SDLE Tea-time Learnings: Data Science (R, Python, Git, Linux, HPC, Hadoop, HBase, Spark)
 
## Authors: A. M. Karimi, R. H. French, in collaboration with the students of the Case Western Reserve University SDLE Research Center 
## Emails: axk962@case.edu, roger.french@case.edu

[Case Western Reserve University, SDLElab] [1]
 
### [SDLE REDCap Sample Database Login] [3]
### [The R Project for Statistical Computing] [4]
### [RStudio Integragrated Development Enviroment (IDE) for R] [5]
### [RMarkdown for open science collaboration & reporting] [6]
### [Frenchrh @frenchrh on Twitter] [7]
### [SDLE_ResCntr @SDLE_ResCntr on Twitter] [8]


[1]: http://sdle.case.edu
[2]: 
[3]: https://dcru.case.edu/redcap/
[4]: http://www.r-project.org/
[5]: http://www.rstudio.com/products/RStudio/
[6]: http://rmarkdown.rstudio.com/
[7]: https://twitter.com/frenchrh
[8]: https://twitter.com/SDLE_ResCntr
[9]: https://www.periscope.tv/SDLE_ResCntr/1RDxlwgLavgJL
[10]: https://www.youtube.com/playlist?list=PLBrrkqzxVsYjiVi9W-3j_bcxgWNq7RqXG

