Teatime topics and Presenter for 2018

June
Week 1 - Introduction to Data Science 
  6-12 - Git, vim, cronjob & Latex [ Yu ]
  6-13 - R - (ggplot, tidyverse) [ Alan ]
  6-14 - Python - (pandas, numpy & matplotlib) [ Jiqi ]

Week 2 - Introduction to CRADLE Environment
  6-19 - Intro to HPC, SQL & NoSQL [ Ahmad ]
  6-20 - Hadoop & HBase [ Wei-Hang]
  6-21 - linux/debian cli & SLURM [ Arash ]

Week 3 - Statistical Learning
  6-26 - Linear Modeling [ Shreyas ]
  6-27 - Multi-variate modeling [ Devin ]
  6-28 - General Additive Models(GAM) [ Donghui ]

July
Week 4 - Open Datasets Acquisition 
  7-03 - Web-scraping [ Phillip ]
  7-04 - Holiday 
  7-05 - Data Science pipeline (data cleaning, pre-processing & results validation) [ Jen ]

Week 5 - Machine Learning Frameworks 
  7-10 - Introduction to Machine Learning [Alan]
  7-11 - Tensorflow & Keras [Ahmad]
  7-12 - Kaggle & Co-Lab [ Menghong ]

Week 6 - Appications
  7-17 - Image Processing [ Jiqi ]
  7-18 - Time Series [ Rojiar ] 
  7-19 - Wavelets [ Phillip ]
